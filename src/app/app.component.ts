import { Component } from '@angular/core';
const AWS = require('aws-sdk');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  outputText = '';
  inputText = '';

  /* tslint:disable */
  sampleDynamoJSON = [
    {
        "stringField": { 
            "s": "this is a string"
        },
        "numberField": {
            "n": "12345"
        }
   }
 ];

 sampleJSON = [
  {
    "stringField": "this is a string",
    "numberField": 12345
  }
 ];
 /* tslint:enable */

 sampleDynamoJsonText = JSON.stringify(this.sampleDynamoJSON, undefined, 2);
 sampleJSONText = JSON.stringify(this.sampleJSON, undefined, 2);

  // From DynamoJSON to JSON
  public unmarshallJSON(): void {
    // Do stuff here
    let myInput: any = [];
    let inputText: string = (<HTMLInputElement>document.getElementById('inputTextarea')).value;
    // Silly thing that AWS pipeline does on Export where it outputs lowercase attribute chars.
    // We have to make them uppercase in order for the AWS SDK API to work.
    inputText = inputText.replace(/"b"/g, function(x) { return x.toUpperCase(); });
    inputText = inputText.replace(/"bool"/g, function(x) { return x.toUpperCase(); });
    inputText = inputText.replace(/"bs"/g, function(x) { return x.toUpperCase(); });
    inputText = inputText.replace(/"l"/g, function(x) { return x.toUpperCase(); });
    inputText = inputText.replace(/"m"/g, function(x) { return x.toUpperCase(); });
    inputText = inputText.replace(/"n"/g, function(x) { return x.toUpperCase(); });
    inputText = inputText.replace(/"ns"/g, function(x) { return x.toUpperCase(); });
    inputText = inputText.replace(/"null"/g, function(x) { return x.toUpperCase(); });
    inputText = inputText.replace(/"s"/g, function(x) { return x.toUpperCase(); });
    inputText = inputText.replace(/"ss"/g, function(x) { return x.toUpperCase(); });

    try {
     myInput = JSON.parse(inputText);

     const resultJSON: any = [];
     for (let x = 0; x < myInput.length; x++) {
        resultJSON.push(AWS.DynamoDB.Converter.unmarshall(myInput[x]));
     }

     document.getElementById('outputTextarea')['value'] = JSON.stringify(resultJSON, undefined, 2);
    } catch (error) {
      // Error Parsing stuff, let the user know.
      console.log(error);
      document.getElementById('outputTextarea')['value'] = 'Oh oh. Make sure your input is a valid DynamoJSON and try again.';
    }
  }

  // This is doing the reverse: from JSON to DynamoJSON
  public marshallJSON(): void {
    const jsonText: string = document.getElementById('outputTextarea')['value'];
    try {
      const toParse: any = JSON.parse(jsonText);
      const resultJSON: any = [];
      for (let x = 0; x < toParse.length; x++) {
        resultJSON.push(AWS.DynamoDB.Converter.marshall(toParse[x]));
     }

     document.getElementById('inputTextarea')['value'] = JSON.stringify(resultJSON, undefined, 2);
    } catch (error) {
      console.error(error);
      document.getElementById('inputTextarea')['value'] = 'Oh oh. Make sure your input is a valid JSON and try again.';
    }
  }

  public clearText(): void {
    document.getElementById('inputTextarea')['value'] = '';
    document.getElementById('outputTextarea')['value'] = '';
  }
}
