# Welcome
![Alt text](src/assets/screenShot.png?raw=true)

# DynamoProject

The purpose of this project is to help:
1. Unmarshall DynamoDB JSON formatted objects into regular JSON objects. 
2. Marshall regular JSON formatted objectes into DynamoDB JSON objects. 

Simply copy and paste either format into the appropriate labeled textarea, and hit that `Unmarshall` or `Marshall` button. =)

## Development server

Run `npm install` to install dependencies.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 

The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Dependencies:
1. AWS Javascript SDK via npm: `aws-sdk`.
2. Material Design via CDN: `https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css`

## Running unit tests

// TODO:
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

Contact Edwin Munguia. 

## Upcoming

1. Implement read from file, and output to file.
2. Make it prettier.  
3. ???
4. Profit.
